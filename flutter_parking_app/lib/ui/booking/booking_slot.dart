import 'package:flutter/material.dart';
import 'package:flutter_parking_app/shared/custom_app_bar.dart';

class BookParking extends StatefulWidget {
  const BookParking({Key key}) : super(key: key);

  @override
  _BookParkingState createState() => _BookParkingState();
}

class _BookParkingState extends State<BookParking> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Book a Ticket",
        arrowBack: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(25.0),
          child: Container(),
        ),
      ),
    );
  }
}
