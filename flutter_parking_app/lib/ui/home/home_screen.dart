import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/model/parking_model.dart';
import '../../core/model/user_model.dart';
import '../../services/firebase_service.dart';
import '../../shared/custom_app_bar.dart';
import '../details/details_parking_screen.dart';
import '../maps/map_location.dart';
import '../profile/profile_user_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  User user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();
  ParkingModel parking;
  FirebaseStorage storage = FirebaseStorage.instance;
  bool isAlowed = false;

  @override
  void initState() {
    super.initState();

    FirebaseFirestore.instance
        .collection("user")
        .doc(user.uid)
        .get()
        .then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());

      setState(() {
        isAlowed = loggedInUser.isClient;
      });
    });
  }

  bool homeColor = false;
  bool profileColor = false;
  bool payColor = false;
  bool historyColor = false;

  @override
  Widget build(BuildContext context) {
    final serviceProvider = Provider.of<FirebaseService>(context);
    return Scaffold(
        key: _key,
        drawer: Drawer(
          child: ListView(children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(
                "Bob",
                style: TextStyle(color: Colors.black),
              ),
              currentAccountPicture: CircleAvatar(
                radius: 25,
                backgroundImage: AssetImage('assets/images/user.png'),
              ),
              decoration: BoxDecoration(color: Color(0xffffa726)),
              accountEmail: Text(
                "Me@gmail.com",
                style: TextStyle(color: Colors.black),
              ),
            ),
            ListTile(
              selected: homeColor,
              onTap: () {
                setState(() {
                  homeColor = true;
                  payColor = false;
                  profileColor = false;
                  historyColor = false;
                });
              },
              leading: Icon(Icons.home_rounded),
              title: Text("Home"),
            ),
            ListTile(
              selected: profileColor,
              onTap: () {
                setState(() {
                  profileColor = true;
                  payColor = false;
                  homeColor = false;
                  historyColor = false;
                });
              },
              leading: Icon(Icons.account_circle_outlined),
              title: Text("Profile"),
            ),
            ListTile(
              selected: payColor,
              onTap: () {
                setState(() {
                  payColor = true;
                  homeColor = false;
                  profileColor = false;
                  historyColor = false;
                });
              },
              leading: Icon(Icons.attach_money_outlined),
              title: Text("Pay"),
            ),
            ListTile(
              selected: historyColor,
              onTap: () {
                setState(() {
                  historyColor = true;
                  payColor = false;
                  profileColor = false;
                  homeColor = false;
                });
              },
              leading: Icon(Icons.article_outlined),
              title: Text("Payment History"),
            ),
            ListTile(
              onTap: () {},
              leading: Icon(Icons.exit_to_app_outlined),
              title: Text("Logout"),
            )
          ]),
        ),
        appBar: CustomAppBar(
          title: "Home",
          actions: [
            IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfileUserScreen(),
                    settings: RouteSettings(
                      arguments: UserModel(
                        uid: user.uid,
                        firstName: loggedInUser.firstName,
                        secondName: loggedInUser.secondName,
                        tag: loggedInUser.tag,
                        email: loggedInUser.email,
                      ),
                    ),
                  ),
                );
              },
              icon: Icon(Icons.person_sharp),
              color: Colors.white,
            ),
            IconButton(
                color: Colors.white,
                onPressed: () {
                  _key.currentState.openDrawer();
                },
                icon: Icon(Icons.menu)),
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                stream: serviceProvider.readParkingItems(),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.data == null) {
                    return Center(child: CircularProgressIndicator());
                  }
                  return ListView.builder(
                    itemCount: snapshot.data.docs.length,
                    itemBuilder: (context, index) {
                      if (snapshot.data == null)
                        return CircularProgressIndicator();
                      DocumentSnapshot data = snapshot.data.docs[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DetailsParkingScreen(),
                                settings: RouteSettings(
                                  arguments: ParkingModel(
                                      id: snapshot.data.docs[index].id,
                                      index: index),
                                ),
                              ),
                            );
                          },
                          child: Card(
                            elevation: 8,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Container(
                                    width: 100,
                                    height: 120,
                                    child: ClipRRect(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                      child: data['image'] != null
                                          ? Image.network(data['image'],
                                              fit: BoxFit.cover)
                                          : Image.asset(
                                              'assets/images/photo_pic.png',
                                              fit: BoxFit.cover),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${data['companyName']}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          '${data['parkingName']}',
                                          maxLines: 2,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          '${data['availableNumOfSlots']} slots available',
                                          maxLines: 2,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.orange),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
        floatingActionButton: isAlowed == true
            ? FloatingActionButton(
                backgroundColor: Colors.orange,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MapLocation()));
                },
                child: Icon(Icons.add),
              )
            : null);
  }
}
